# AZ-104 Azure Administrator Training <!-- omit in toc -->

contents
- [Course](#course)
- [What you'll learn](#what-youll-learn)

# Sections <!-- omit in toc --> 
- [Section 1](/Cert%20Training/AZ-104%20Azure%20Administrator/Sections/Section1/)
  - [Notes](/Cert%20Training/AZ-104%20Azure%20Administrator/Sections/Section1/README.md)
- [Section 2](/Cert%20Training/AZ-104%20Azure%20Administrator/Sections/Section2/)
  - [Notes](/Cert%20Training/AZ-104%20Azure%20Administrator/Sections/Section2/README.md)
- [Section 3](/Cert%20Training/AZ-104%20Azure%20Administrator/Sections/Section3/)
  - [Notes](/Cert%20Training/AZ-104%20Azure%20Administrator/Sections/Section3/README.md)
  
# Course

[Udemy](https://www.udemy.com/course/70533-azure/)
[Instructor - Scott Duffy](https://www.udemy.com/course/70533-azure/#instructor-1)

# What you'll learn
- Know how to implement solutions for the Microsoft Azure platform
- Pass the Microsoft AZ-104 Microsoft Azure Administrator test the first time
- Achieve the Azure Administrator Associate badge
- Understand the main concepts of Azure, beyond the ones you normally use
- Be up-to-date on the latest updates to this ever-changing platform
