**examplenote**

contents

- [assets](#assets)
- [notes](#notes)
- [Example](#example)

# assets

# notes

# Example

You can break out notes into sections and reference them in the section's main `<section>.md` filewith:

`[note name](notes/notename.md)`