# Section Example <!-- omit in toc -->

contents

- [assets](#assets)
  - [assets directory](#assets-directory)
  - [example_asset.sh](#example_assetsh)
- [notes](#notes)
- [links](#links)

# assets

## [assets directory](assets/)
## [example_asset.sh](assets/example_asset.sh)

# notes

# links

1. [Internal link](/Cert%20Training/AZ-104%20Azure%20Administrator/README.md)
2. [Resource2](https://example.com/link1)
   1. https://example.com/link1

## example <!-- omit in toc -->
This section will not appear in the table of contents