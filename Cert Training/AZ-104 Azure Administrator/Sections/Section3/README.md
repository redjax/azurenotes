# Section 3 <!-- omit in toc -->

contents

- [assets](#assets)
  - [assets directory](#assets-directory)
- [notes](#notes)
  - [Programming/scripting on the exam](#programmingscripting-on-the-exam)
    - [Azure Portal](#azure-portal)
  - [Memorizing PowerShell/CLI commands](#memorizing-powershellcli-commands)
  - [Details about PowerShell/CLI for Managing Azure](#details-about-powershellcli-for-managing-azure)
  - [Installing PowerShell Core with AZ Module](#installing-powershell-core-with-az-module)
  - [Switching to Another Subscription](#switching-to-another-subscription)
- [links](#links)

# assets

## [assets directory](assets/)

# notes

## Programming/scripting on the exam

- There is no programming on the exam
  - There is some light scripting

- 3 ways to access resources
  - [Azure Portal](#azure-portal)
  - PowerShell Az
  - Bash/CLI

- There are also some JSON/CLI responses on the exam

- How does this appear on the test?
  - Method 1: Performance based testing ("Labs")
    - **Note**: Labs have not appeared on recent Azure exams
    - Fires up an instance of Azure
    - Asks you to perform a set of tasks
    - Can perform task any way you know how
      - Portal
      - PowerShell
      - Bash
  - Method 2: Code in question
    - Question/answer will have some code, and you will determine if the code performs a required task
      - Might also have code with missing code, and you have to type/choose the missing code

- Questions will always use PowerShell (not Bash) on the exam.

### Azure Portal

- Access at [portal.azure.com](https://portal.azure.com)

- **Azure Cloud Shell**
- A command line interface within the browser
  - Open with Cloud Shell icon:
  
  ![cloud shell](assets/cloud_shell.png)

  - You will be prompted to set up a storage instance just for the cloud shell
  - Can choose between Bash/PowerShell

  ![cloud shell type](assets/cloud_shell_type.png)

  - After choosing your initial cloud type, you can live-switch shells using the dropdown in the top left of the cloud shell window
  
  ![cloud shell picker](assets/cloud_shell_picker.png) 

## Memorizing PowerShell/CLI commands

- There is a predicatble naming system for commands

- CLI
  - There is an [Azure Bash/CLI Reference](https://docs.microsoft.com/en-us/cli/azure/reference-index?view=azure-cli-latest) page on Microsoft's docs site
  - Commands start with `az`
    - i.e. list Azure VMs: `az vm list`
    - i.e. create Azure VM: `az vm create`
    - i.e. list keyvault: `az keyvault list`
    - i.e. list network virtual networks: `az network vnet list`
      - `vnet` is a sub-command of `az network`
- PowerShell
  - There is an [Azure PowerShell Reference](https://docs.microsoft.com/en-us/powershell/module/?view=azps-7.5.0) page on Microsoft's docs site
    - Broken down by PowerShell Module, i.e. `Az.Accounts` module
  - Commands start with `$Verb-Az{module}`
    - i.e. list Azure VMs: `Get-AzVM`
    - i.e. create Azure VM: `New-AzVM`
    - i.e. list keyvault: `Get-AzKeyvault`
    - i.e. list network virtual networks: `Get-AzVirtualNetwork`
      - i.e. list subnet config: `Get-AzVirtualNetworkSubnetConfig`

## Details about PowerShell/CLI for Managing Azure

- Allows running Azure commands from your desktop, via PowerShell
- Get the latest version of PowerShell
  - [Install docs](https://docs.microsoft.com/en-us/powershell/scripting/install/installing-powershell-on-windows?view=powershell-7.2)
  - Install with `winget`
    - `winget search Microsoft.PowerShell`
    - This should return results for the most recent version of PowerShell, i.e.:
    
    ```
    Name               Id                           Version Source
    ---------------------------------------------------------------
    PowerShell         Microsoft.PowerShell         7.2.3.0 winget
    PowerShell Preview Microsoft.PowerShell.Preview 7.3.0.3 winget
    ```

    - Install PowerShell using the `id` parameter:
    
    `winget install --id Microsoft.PowerShell --source winget` 

- There are Azure-specific modules for PowerShell/CLI
  - PowerShell
    - Microsoft switched module from `AzureRM` to `Az`
      - Old references to `AzureRM` must be replaced with `Az` command
    - You cannot install `AzureRM` and `Az` installed simultaneously
    - Upgrading
      - Upgrade a minor version (i.e. `5.6.0` -> `5.7.0`):
        - (as admin) `Install-Module -Name Az -AllowClobber   -Repository PSGallery`
      - Upgrade a major version (i.e. `5.6.0` -> `6.3.0`):
        - (as admin) `Install-Module -Name Az -AllowClobber -Repository PSGallery -Force`
  - When running Azure commands locally with PowerShell, you must login before running any commands
    - `Connect-AzAccount`
  - You can develop scripts locally and push/store them in Azure Cloud Shell

## Installing PowerShell Core with AZ Module

- [Link to PowerShell Github](https://github.com/PowerShell/PowerShell)
  - **NOTE**: Only do this if you are not installing with another method, i.e. `winget`
  - Use "tags" to download a release
  
  ![github tags](assets/powershell_github_tags_link.png)

  - Switch to `Releases` to find the download
  
  ![github tag releases](assets/powershell_github_releases_toggle.png)
  
  - In the list, scroll to the release for your platform (i.e. 
  
  `powershell-<version>-win-x64.msi`)
  
  ![github release download](assets/powershell_github_release_download.png)


- Run PowerShell as an administrator
  - Check that you are on the correct version
    `$PSVersionTable.PSVersion`
    - If you do not see the version you installed from `winget` or Github, search your PC for that version (i.e. for PowerShell 7, open the `PowerShell 7` app on your PC)
    
    ![powershell 7 launcher](assets/powershell_launcher.png)

  - Install the `Az` module
    
    -AllowClobber: Allows installation to overwrite existing files

    -Force: Allows installing over existing `Az` module

    ```
    Install-Module -Name Az -AllowClobber -Force
    ```

    - You can check the versions of `Az`  you have installed with:

    ```
    Get-InstallModule -Name Az -AllVersions | Select-Object -Property Name, Version
    ```

  - Connect to Azure
  
  ```
  Connect-AzAccount
  ```

    - You will either get a code to enter, or a browser popup to sign in, which will send your authentication back to the script


## Switching to Another Subscription

- Check subscriptions with:
  
  ```
  Get-AzSubscription
  ```

  - This returns all subscriptions associated with your account
  - Switch to another subscription:

  ```
  $context = Get-AzSubscription -SubscriptionId <subscription ID>

  Set-AzContext $context
  ```

# links

- [Azure Portal](https://portal.azure.com)
- [Azure Bash/CLI Reference](https://docs.microsoft.com/en-us/cli/azure/reference-index?view=azure-cli-latest)
- [Azure PowerShell Reference](https://docs.microsoft.com/en-us/powershell/module/?view=azps-7.5.0)
- [PowerShell Install docs](https://docs.microsoft.com/en-us/powershell/scripting/install/installing-powershell-on-windows?view=powershell-7.2)
- [PowerShell Github](https://github.com/PowerShell/PowerShell)
