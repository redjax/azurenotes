# <!-- omit in toc -->Section 1

contents
- [assets](#assets)
  - [assets directory](#assets-directory)
  - [AZ-104 Official Course Study Guide](#az-104-official-course-study-guide)
  - [AZ-104 Sample Questions](#az-104-sample-questions)
  - [Exam skills measure](#exam-skills-measure)
- [links](#links)
  - [Landing page for AZ-104 on Microsoft Learn](#landing-page-for-az-104-on-microsoft-learn)
- [notes](#notes)
  - [Section 1 Video Notes](#section-1-video-notes)
  - [Amazing Microsoft Resources (end of section 1)](#amazing-microsoft-resources-end-of-section-1)

# assets

## [assets directory](/Cert%20Training/AZ-104%20Azure%20Administrator/Sections/Section1/assets/)
## [AZ-104 Official Course Study Guide](assets/AZ-104+Official+Course+Study+Guide.pdf)
## [AZ-104 Sample Questions](assets/AZ-104-SampleQuestions.pdf)
## [Exam skills measure](assets/exam-az-104-microsoft-azure-administrator-skills-measured.pdf)

# links

## [Landing page for AZ-104 on Microsoft Learn](https://docs.microsoft.com/en-us/learn/certifications/exams/az-104)

   * https://docs.microsoft.com/en-us/learn/certifications/exams/az-104
   * Shows most recent updates
   * Free sample questions download
   * Schedule exam

# notes

## [Section 1 Video Notes](https://www.udemy.com/course/70533-azure/learn/lecture/11783624#notes)

## Amazing Microsoft Resources (end of section 1)

- [Microsoft Learn AZ-104](https://docs.microsoft.com/en-us/learn/certifications/exams/az-104#two-ways-to-prepare)
   - https://docs.microsoft.com/en-us/learn/certifications/exams/az-104#two-ways-to-prepare
- [Azure Samples](https://docs.microsoft.com/en-us/samples/browse/?products=azure)
   - https://docs.microsoft.com/en-us/samples/browse/?products=azure
- [Azure SDK](https://azure.microsoft.com/en-us/downloads/)
   - https://azure.microsoft.com/en-us/downloads/
- [Github - Azure Powershell](https://github.com/Azure/azure-powershell)
   - https://github.com/Azure/azure-powershell
- [Citadel](https://azurecitadel.com)
   - https://azurecitadel.com
   - Hands on Azure learning
- [Microsoft Cloud Workshop](https://microsoftcloudworkshop.com)
   - https://microsoftcloudworkshop.com
   - Hands on Azure learning
- [Microsoft Learn Content Directory - AZ-104-MicrosoftAzureAdministrator](https://microsoftlearning.github.io/AZ-104-MicrosoftAzureAdministrator/)
   - https://microsoftlearning.github.io/AZ-104-MicrosoftAzureAdministrator/
   - Content from the Microsoft Learn page for Azure Administrator