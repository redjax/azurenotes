# Section 2 <!-- omit in toc -->

contents

- [assets](#assets)
  - [assets directory](#assets-directory)
- [notes](#notes)
  - [Virtual Machines](#virtual-machines)
    - [Abstractions](#abstractions)
    - [App Services](#app-services)
  - [Virtual Networking](#virtual-networking)
    - [networking categories](#networking-categories)
      - [Network Connectivity](#network-connectivity)
      - [Network Security](#network-security)
      - [Network Delivery](#network-delivery)
      - [Network Monitoring](#network-monitoring)
  - [Storage](#storage)
    - [Data Services - SQL Server Related](#data-services---sql-server-related)
    - [Data Services - Other](#data-services---other)
  - [Microservices](#microservices)

# assets

## [assets directory](assets/)

# notes

**Overview of Azure's services**

Core services

[Virtual Machines](#virtual-machines)

[Virtual Networking](#virtual-networking)

[Storage](#storage)

Core services are the "building blocks" for the rest of Azure. Virtual machines host applications, use virtual networking for communication, and have storage.

## Virtual Machines

* A virtualized server running a host OS
* Can be accessed via RDP/SSH/etc
* **MUST BE** on a virtual network
  * Can be behind load balancer, vpn, etc
* Can also be arranged in "availability sets"

### Abstractions

* Azure Batch
* Virtual Machine Scale Sets
* Azure Kubernetes Service (AKS)
* Service Fabric

### App Services

* Web apps or container apps
* Windows/Linux
  * You don't control/manage host OS
  * Fully managed
* .NET, .NET Core, Java, Ruby, Node.js, PHP, & Python
* Benefits in scaling, CI, deployment slots, integrates with Visual Studio

## Virtual Networking

### networking categories

* [connectivity](#network-connectivity)

* [security](#network-security)

* [delivery](#network-delivery)

* [monitoring](#network-monitoring)

#### Network Connectivity

* Virtual Network (VNet)
  * Normal networking (cables, routers, switches, etc), but virtualized
  * Just a database entry
* Virtual WAN
  * Allows offices to connect to each other
* ExpressRoute
  * Faster way to connect to Azure
  * Costs more
  * Encrypted
  * Way faster
  * Private Network
* VPN Gateway
  * Point-to-Site
  * Site-to-Site
* Azure DNS
  * Private/Public domain names managed by Azure nameservers
* Peering
  * Connect virtual networks to each other
  * Allows inter-regional communication
* Bastion
  * Allows RDP without opening RDP ports

#### Network Security

* Network Security Groups (NSG)
  * Access Control List (ACL)-style policies
* Azure Private Link
  * Turns private endpoints into public & connect
* DDoS Protection
* Azure Firewall
* Web Application Firewall (WAF)
  * Built into Application Gateway
  * Application for Azure Frontdoor
  * Can recognize top 10/top 20 common attacks
* Virtual Network Endpoints

#### Network Delivery

Traffic shaping

* CDN
* Azure Front Door
* Traffic Manager
* Application Gateway
  * L7 application load balancer
* Load Balancer
  * L4 transport layer load balancer

#### Network Monitoring

Azure is not your network, which can complicate monitoring. There are tools built into Azure to help with monitoring/watching/debug taffic

* Network Watcher
* ExpressRoute Monitor
* Azure Monitor
* VNet Terminal Access Point (TAP)

## Storage

* Create storage accounts up to 5PB each
  * Don't cost to create
  * Charge per GB
    * Approx. $0.018/GB
* Blobs, queues, tables, files
  * Blobs are most common
    * Stored in containers
  * Azure keeps 3 copies of your files for DR
* Various levels of replication included from local to global
* Storage tiers
  * Hot
  * Cool
  * Archive
* Managed (for VMs) or unmanaged

### Data Services - SQL Server Related

**NOTE**: Not covered in this course

* Azure SQL database
* Azure SQL Managed Instance
  * Between a SQL server on a VM and a database in Azure
  * Managed instance
* SQL Server on a VM
* Synapse Analytics (SQL Data Warehouse)
  * Storing multiple PBs of data
  * Designed to be queried, not for transactions

### Data Services - Other

* Cosmos DB
  * Most prominent non-SQL database
  * NoSQL, non-relational
  * Store data like MongoDB/DocumentDB/GraphAPI
  * Stores JSON objects
* Azure Database for MySQL
* Azure Database for PostgreSQL
* Azure Database for MariaDB
* Azure Cache for Redis
  * Application stores data temporarily for sessions

## Microservices

* Service Fabric
  * Another way of managing applications running in a VM
* Azure Functions
  * Small pieces of code
  * Can be edited in browser
* Azure Logic Apps
  * Workflow-type services
  * Apps walk through logical "steps"
* API Management
  * Sits in front of an API
  * Secure, throttle, modify data as it comes/goes
* Azure Kubernetes Service (AKS) - containerized app
  * Create container app and run it in Kubernetes
