# Azure Notes

## Description

A place for me to store Markdown-formatted notes related to Azure.

### Git branches

- `main`
  - Main branch
  - Updates are merged into `stage`, then a merge/pull request is made for `stage --> main`
- `stage`
  - After branching (i.e. to make new section notes, like `notes/section3`), make changes and push.
  - Open a merge/pull request from i.e. `notes/section3` into `stage`
  - When all new notes are ready to be merged into `main`, open a merge/pull request, `stage --> main`
- `notes/<section>`
  - When starting a new section, i.e. `Section 3`
    - Create a new branch `notes/section3`
    - Take notes, add assets, etc
    - Push `notes/section3` back to Git
    - Merge `notes/section3 --> stage`
      - Delete branch if done taking notes for `Section 3`
    - If no conflicts, merge `stage --> main`

## Sections

* [Cert Training/](Cert%20Training/)
  * Any certification training notes I take, i.e. Udemy courses or Microsoft Learn
  * [AZ-104 Azure Administrator](Cert%20Training/AZ-104%20Azure%20Administrator/)
    * [Sections](Cert%20Training/AZ-104%20Azure%20Administrator/Sections)
      * [Section1](Cert%20Training/AZ-104%20Azure%20Administrator/Sections/Section1)
        * Course overview
        * Setting up Azure free tier
        * Setting up budget
      * [Section2](Cert%20Training/AZ-104%20Azure%20Administrator/Sections/Section2)
        * Azure concepts
        * Overviews
          * Azure VM & App Services
          * Storage & Data Services
          * Networking Services & Microservices
      * [Section3](/Cert%20Training/AZ-104%20Azure%20Administrator/Sections/Section3/)
        * Programming/scripting questions on the exam
        * PowerShell/CLI commands
        * Managing Azure with Powershell/CLI/Azure Cloud Shell
        * Installing PowerShell (core)
        * Installing/using `Az` module